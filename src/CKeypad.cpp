#include <Arduino.h>
#include "CKeypad.h"

using MoLocker::CKeypad;

CKeypad::~CKeypad()
{
    delete keypadLibrary;
}

void CKeypad::setRowPins(Pin pins[])
{    
    //
}

void CKeypad::setColumnPins(Pin pins[])
{    
    //
}

void CKeypad::setup()
{   
    char keymap[4][4] = {
        {'1', '2', '3', 'A'},
        {'4', '5', '6', 'B'},
        {'7', '8', '9', 'C'},
        {'*', '0', '#', 'D'},
    };

    uint8_t rowPins[] = {13, 12, 11, 10};
    uint8_t columnPins[] = {9, 8, 7, 6};

    uint8_t rowCount = 4;
    uint8_t columnCount = 4;

    keypadLibrary = new Keypad(
        makeKeymap(keymap),
        rowPins,
        columnPins,
        rowCount,
        columnCount
    );

    // Must be called once in the beginning for some odd reason
    // keypadLibraryd->getKey();
}

bool CKeypad::isKeyPressed()
{ 
    return (lastPressedKey != NO_KEY); 
}

bool CKeypad::isKeyPressed(char key)
{
    return (lastPressedKey == key);
}

char CKeypad::pressedKey()
{
    return lastPressedKey;
}

void CKeypad::update()
{
    lastPressedKey = keypadLibrary->getKey();
}
