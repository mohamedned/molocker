#include <cantino.h>
#include "Piezo.h"
#include "CKeypad.h"

using namespace cantino;

using MoLocker::Piezo;
using MoLocker::CKeypad;

int main()
{
    cout << "Program started" << endl;

    Piezo piezo(3);
    piezo.setBeepFrequency(2960);
    piezo.setBeepDuration(150);

    CKeypad keypad;

    uint8_t keypadRowPins[] = {13, 12, 11, 10};
    uint8_t keypadColumnPins[] = {9, 8, 7, 6};
    
    keypad.setRowPins(keypadRowPins);
    keypad.setColumnPins(keypadColumnPins);

    keypad.setup();

    while (true) {
        if (keypad.isKeyPressed()) {
            cout << keypad.pressedKey() << endl;
        }
        
        if (keypad.isKeyPressed('A')) {
            piezo.beep();
        }

        keypad.update();
    }

    return 0;
}
