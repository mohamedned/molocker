#include <Arduino.h>
#include "Piezo.h"

using MoLocker::Piezo;

Piezo::Piezo(Pin pin) 
    : pin(pin) // this->pin = pin;
{
    pinMode(pin, OUTPUT);
}

void Piezo::setBeepFrequency(Hertz frequency)
{
    beepFrequency = frequency;
}

void Piezo::setBeepDuration(Milliseconds time)
{
    beepDuration = time;
}

void Piezo::beep()
{
    tone(pin, beepFrequency, beepDuration);
    delay(beepDuration * 1.3);
    noTone(pin);
}
