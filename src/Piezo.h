#pragma once

#include <stdint.h>

namespace MoLocker
{
    class Piezo 
    {
        private:
            using Pin = uint8_t;
            using Milliseconds = uint16_t;
            using Hertz = uint16_t;

            Pin pin;
            Hertz beepFrequency;
            Milliseconds beepDuration;

        public:
            Piezo(Pin pin);

            void setBeepFrequency(Hertz frequency);
            void setBeepDuration(Milliseconds time);

            void beep();
    };
}
