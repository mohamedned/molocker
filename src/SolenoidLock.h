#pragma once

#include <stdint.h>

namespace MoLocker
{
    class SolenoidLock
    {
        private:
            using Pin = uint8_t;
            using Milliseconds = uint16_t;

            Pin pin;

        public:
            SolenoidLock(Pin pin);
            
            void open();
            void close();

            void openAndCloseAfter(Milliseconds time);
    };
}
