#include <Arduino.h>
#include "SolenoidLock.h"

using MoLocker::SolenoidLock;

SolenoidLock::SolenoidLock(Pin pin)
    : pin(pin) // this->pin = pin;
{
    pinMode(pin, OUTPUT);
}

void SolenoidLock::open()
{
    digitalWrite(pin, HIGH);
}

void SolenoidLock::close()
{
    digitalWrite(pin, LOW);
}

void SolenoidLock::openAndCloseAfter(Milliseconds time)
{
    open();
    delay(time);
    close();
}
