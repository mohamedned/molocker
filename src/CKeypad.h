#pragma once

#include <stdint.h>
#include <Keypad.h>

namespace MoLocker
{
    class CKeypad
    {
        private:
            using Pin = uint8_t;

            // Pin rowPins[];
            // Pin colPins[];

            Keypad *keypadLibrary;
            char lastPressedKey;

        public:
            ~CKeypad();

            void setRowPins(Pin pins[]);
            void setColumnPins(Pin pins[]);
            
            void setup();
            void update();

            // function overloading: same name, diff. params
            bool isKeyPressed();
            bool isKeyPressed(char key);

            char pressedKey();
    };
}
